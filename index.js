// Javascript Selection Control Structures (Activity S19)

// console.log("Hello Mader Fader!");

console.log("Welcome");

/*
    1. Declare 3 variables without initialization called username,password and role.
*/

// let day = prompt('What day of the week is it today?').toLowerCase();
let userType = 'Not Logged In';
let userName;
let userPassword;
let userRole;

console.log(userType);





/*function userType () {

		let userName = prompt ("Enter your User Name: ");
		let userPassword = prompt ("Enter your Password: ");
		let userRole = prompt ("Enter your Role: ");

		console.log ("Hello, " + userRole +" "+ userName );
		
}
userType();
*/

/*
    
    2. Create a login function which is able to prompt the user to provide their username, password and role.
        -use prompt() and update the username,password and role global variable with the prompt() returned values.
        -add an if statement to check if the the username is an empty string or null or if the password is an empty string or null or if the role is an empty string or null.
            -if it is, show an alert to inform the user that their input should not be empty.
        -add an else statement. Inside the else statement add a switch to check the user's role add 3 cases and a default:
                -if the user's role is admin, show an alert with the following message:
                    "Welcome back to the class portal, admin!"
                -if the user's role is teacher, show an alert with the following message:
                    "Thank you for logging in, teacher!"
                -if the user's role is a rookie, show an alert with the following message:
                    "Welcome to the class portal, student!"
                -if the user's role does not fall under any of the cases, as a default, show a message:
                    "Role out of range."
*/
userName = prompt ("Enter username: ").toLowerCase();
if (userName==""){
    alert('empty');
    }

    userPassword = prompt ("Enter Password: ").toLowerCase();
    if (userPassword==""){
        alert('empty');
    }
    userRole = prompt ("Enter Role:\nAdmin\nTeacher\nRookie\n ").toLowerCase();
    if (userRole==""){
        alert('empty');
    }
    else if (userRole== "admin") {
    alert ("Welcome back to the class portal!, " + userRole +" "+ userName );
        // return 'Admin';
    }
    else if (userRole== 'teacher') {
        alert ("Welcome back to the class portal!, " + userRole +" "+ userName );
        // return 'Teacher';
    }
    else if (userRole== 'rookie') {
    alert ("Welcome back to the class portal!, Student  "+ userName );
    // return 'Rookie';

    }
    else {
        // return 'Role out of Range';
        console.log( 'Role out of Range')
    }



 /*
    3. Create a function which is able to receive 4 numbers as arguments calculate its average and log a message for  the user about their letter equivalent in the console.
        -add parameters appropriate to describe the arguments.
        -create a new function scoped variable called average.
        -calculate the average of the 4 number inputs and store it in the variable average.
        -research the use of Math.round() and round off the value of the average variable.
            -update the average variable with the use of Math.round()
            -console.log() the average variable to check if it is rounding off first.
        -add an if statement to check if the value of average is less than or equal to 74.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is F"
        -add an else if statement to check if the value of average is greater than or equal to 75 and if average is less than or equal to 79.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is D"
        -add an else if statement to check if the value of average is greater than or equal to 80 and if average is less than or equal to 84.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is C"
        -add an else if statement to check if the value of average is greater than or equal to 85 and if average is less than or equal to 89.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is B"
        -add an else if statement to check if the value of average is greater than or equal to 90 and if average is less than or equal to 95.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A"
        -add an else if statement to check if the value of average is greater than 96.
            -if it is, show the following message in a console.log():
            "Hello, student, your average is <show average>. The letter equivalent is A+"

        Invoke and add a number as argument using the browser console.

        Stretch goal:
            -Add an if statement to check the role of the user.
                -if the role of the user is currently "teacher" or "admin" or undefined or null, show an alert:
                "<role>! You are not allowed to access this feature!".
                -else, continue to evaluate the letter equivalent of the student's average.

                gr

// */
// let sub1;
// let sub2 ;
// let sub3 ;
// let sub4 ; 

function givenGrade(sub1 , sub2 , sub3 , sub4){

    let averageGrade = (sub1 + sub2 + sub3 + sub4) / 4 ;

      averageGrade = Math.round(averageGrade)

    if(userRole=== "admin" || userRole=== "teacher" ||userRole=== "undefined" || userRole==="Null"){
        alert("You are not allowed to access this feature");
    }
    

  else{

            if (averageGrade <=74){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is F");
            }

            else if (averageGrade <=89 && averageGrade >=75){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is E");
           
            }
            else if (averageGrade <=84 && averageGrade >=80){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is D");
           
            }
            else if (averageGrade <=81 && averageGrade >=85){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is C");
           
            }
            else if (averageGrade <=86 && averageGrade >=90){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is B");
           
            }
            else if (averageGrade <=91 && averageGrade >=95){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is A");
           
            }
            else if  (averageGrade <=96 && averageGrade >=100){
                console.log ("Hello, student, your average  "  + averageGrade + ". The letter equivalent is A+");
           
            }
            else{
                console.log ("Grade is Out of Range");
           
            }


            
        }


}
givenGrade( 90,89,90,88  );

// if (userRo le=='rookie'){
//         console.log(userRole + "! You are allowed to access this feature!");
//     }
//     else  {
//     alert userRole + "You are not allowed to access this feature!";
//         // return 'Admin';
//     }
//    /* else if (userRole== 'teacher') {
//         console.log ("Welcome back to the class portal!, " + userRole +" "+ userName );
//         // return 'Teacher';
//     }
//     else if (userRole== 'Rookie') {
//      console.log ("Welcome back to the class portal!, Student  "+ userName );
//     // return 'Rookie';

//     }
//     else {
//         // return 'Role out of Range';
//         console.log( 'Role out of Range')
//     }*/


// let message = 'No message';
// console.log(message);

// // You can use 'if-else' statements inside functions by using the function's parameter (wind_speed) within the condition of the if-else statements.
// function determineGrade(grade) {
//     if(grade = < 30) {
//         return 'Not a typhoon yet';
//     } else if (wind_speed <= 61) {
//         return 'Tropical depression detected';
//     } else if (wind_speed >= 62 && wind_speed <= 88) {
//         return 'Tropical Storm detected';
//     } else {
//         return 'Typhoon detected!';
//     }
// }

// message = determineTyphoonIntensity(69);
// console.log(message);

